//
//  KeyboardShowBehaviorViewController.swift
//  TomskSoftNotification
//
//  Created by ora on 6/18/18.
//  Copyright © 2018 ora. All rights reserved.
//

import UIKit

class KeyboardShowBehavior {
    private var keyboardMargin: CGFloat = 10;
    private var keyboardDismissTap: UITapGestureRecognizer?;
    private var keyboardShowFunctionList: [(_ offsetSize: CGFloat)->()] = [];
    private var keyboardHideFunctionList: [()->()] = [];
    
    init(_ keyboardMargin: CGFloat?) {
        self.registerKeyboardNotifications();
        
        guard let keyboardMargin = keyboardMargin else {return}
        self.keyboardMargin = keyboardMargin
    }
    
    deinit {
        self.deregisterKeyboardNotifications();
    }
    
    private func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    public func deregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let offsetSize: CGFloat = keyboardSize.height + self.keyboardMargin
            for callBack in self.keyboardShowFunctionList {
                callBack(offsetSize)
            }
        }
    }
    
    public func addKeyboardShowCallback(_ callBack: @escaping ((_ offsetSize: CGFloat)->Void) ) -> Void {
        self.keyboardShowFunctionList.append(callBack)
    }
    
    public func addKeyboardHideCallback(_ callBack: @escaping (()->Void) ) -> Void {
        self.keyboardHideFunctionList.append(callBack)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        for callBack in self.keyboardHideFunctionList {
            callBack()
        }
    }
}
