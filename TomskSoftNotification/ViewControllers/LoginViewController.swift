//
//  ViewController.swift
//  TomskSoftNotification
//
//  Created by ora on 17/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import UIKit
import SnapKit

class LoginViewController: UIViewController {

    var contentView : LoginView!;
    var loginController:LoginController = LoginController();
    var keyboardShowBehavior:KeyboardShowBehavior!;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        contentView = LoginView()
        contentView.initialise()

        contentView.contentSize = view.bounds.size
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalTo(view)
        }
        
        contentView.loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        
        guard let user = User() else {
            return;
        }
        
        if let notificationViewController = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewControllerID") as? NotificationViewController {
            notificationViewController.loginUser = user
            self.navigationController?.pushViewController(notificationViewController, animated: false)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }


    @objc func loginButtonTapped(_ sender: UIButton) {
        contentView.loginButton.isEnabled=false;
        contentView.hideError();

        guard let login = contentView.loginTxt.text else { contentView.setError("Please, enter your login"); return; }
        guard let password = contentView.passwordTxt.text else { contentView.setError("Please, enter your password"); return; }

        if(login.count == 0){
            contentView.setError("Please, enter your login");
            contentView.loginButton.isEnabled=true;
            return;
        }

        if (password.count == 0){
            contentView.setError("Please, enter your password");
            contentView.loginButton.isEnabled=true;
            return;
        }

        self.loginController.login(login, password) { [weak self] (result: Result<Bool>) -> Void in
                self?.contentView.loginButton.isEnabled = true;

                switch result {
                    case .Success:
                        guard let loginUser = self?.loginController.loginUser else {
                            guard let errorDescription = Errors.UserError.AuthorizationError.errorDescription else {
                                self?.contentView.setError("Unknown error")
                                return;
                            }
                            self?.contentView.setError(errorDescription);
                            return;
                        }

                        self?.contentView.loginTxt.text = ""
                        self?.contentView.passwordTxt.text = ""
                        if let notificationViewController = self?.storyboard?.instantiateViewController(withIdentifier: "NotificationViewControllerID") as? NotificationViewController {
                            notificationViewController.loginUser = loginUser
                            self?.navigationController?.pushViewController(notificationViewController, animated: true)
                        }

                    case .Error(let error):
                        self?.contentView.setError(error.localizedDescription);
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
        // Dispose of any resources that can be recreated.
    }
}




