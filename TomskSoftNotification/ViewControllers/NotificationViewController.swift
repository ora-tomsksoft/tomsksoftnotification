//
//  MainViewController.swift
//  TomskSoftNotification
//
//  Created by ora on 18/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import UIKit
import Locksmith

class NotificationViewController: UIViewController, UITextViewDelegate {
 
    @IBOutlet weak var notificationTable: UITableView!
    @IBOutlet weak var sendNotificationButton: UIButton!
    @IBOutlet weak var delayTextElement: UITextView!
    @IBOutlet weak var delayDateLabel: UILabel!
    @IBOutlet weak var delayDatePicker: UIDatePicker!
    @IBOutlet weak var errorLabel: UILabel!
    
    var delayList: [Notification] = [];
    private let dateformatToStr = DateFormatter();
    private var user: User?;
    private var keyboardHalfHeight: CGFloat = 0;
    private var keyboardShowBehavior:KeyboardShowBehavior!;
    private let userDefaults = UserDefaults.standard
    private var calendarDismissTapRecognizer: UITapGestureRecognizer?;
    private let keyboardMargin: CGFloat = 10;
    private var keyboardDismissTap: UITapGestureRecognizer?;
    
    var loginUser: User! {
        set { if let user = newValue {self.user = user}}
        get { return self.user}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.dateformatToStr.dateFormat = "yyyy-MM-dd";
        self.calendarDismissTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissCalendar));
        
        if let data = UserDefaults.standard.value(forKey: "notifications") as? Data {
            if let notifications = try? PropertyListDecoder().decode(Array<Notification>.self, from: data) {
                self.delayList = notifications
                self.notificationTable.reloadData()
            }
        }
        
        self.delayDatePicker.isHidden = true;
        self.delayTextElement.delegate = self
        
        self.delayDateLabel.text = self.dateformatToStr.string(from: Date())
        
        self.notificationTable.layer.borderWidth = 1.0;
        self.notificationTable.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor;
        
        self.delayTextElement.layer.borderWidth = 1.0;
        self.delayTextElement.layer.borderColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1).cgColor;
        
        self.keyboardShowBehavior = KeyboardShowBehavior(0)
        self.keyboardShowBehavior.addKeyboardShowCallback(self.getKeyboardOpenFunction())
        self.keyboardShowBehavior.addKeyboardHideCallback(self.getKeyboardHideFunction())
    }

    @objc func dismissCalendar() {
        self.delayDatePicker.isHidden = true;
        self.delayTextElement.isHidden = false;
        
        if let calendarDismissTapRecognizer = self.calendarDismissTapRecognizer {
            self.view.removeGestureRecognizer(calendarDismissTapRecognizer)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.navigationController?.navigationBar.barTintColor = self.view.tintColor
        
        let uiBarButtonItem = UIBarButtonItem(image: UIImage(named: "settings-gear-icon"), style: .plain, target: self, action: #selector(self.settingsTapped))
        self.navigationItem.rightBarButtonItem = uiBarButtonItem
    }
    
    private func sendButtonEnableControl(){
        guard let delay_text = self.delayTextElement.text, !delay_text.isEmpty, let data_text = self.delayDateLabel.text, !data_text.isEmpty else {
            self.disableSendButton();
            return;
        }
        
        self.enableSendButton();
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.sendButtonEnableControl();
    }
    
    // *** Calendar Region *** ///
    
    @IBAction func calendarButtonTapped(_ sender: UIButton) {
        if let calendarDismissTapRecognizer = self.calendarDismissTapRecognizer {
            self.view.addGestureRecognizer(calendarDismissTapRecognizer)
        }
        
        self.delayDatePicker.isHidden = !self.delayDatePicker.isHidden;
        self.delayTextElement.isHidden = !self.delayDatePicker.isHidden;
    }
    
    @IBAction func dateValueChange(_ sender: UIDatePicker) {
        self.delayDateLabel.text = self.dateformatToStr.string(from: sender.date)
        self.sendButtonEnableControl();
    }

    // *** Send Notification Button Region *** //
    
    @IBAction func sendNotifiactionTapped(_ sender: UIButton) {
        self.dismissKeyboard()
        guard let text = self.delayTextElement.text, let date_txt = self.delayDateLabel.text else {return}
        
        if (self.delayList.count > 0 && self.delayList.first?.message == text){
            return;
        }
        
        guard let date = self.dateformatToStr.date(from: date_txt) else {return}
        let notification = Notification(date: date, message: text)
        
        self.errorLabel.isHidden = true;
        self.disableSendButton();
        
        self.user?.sendNotification(notification, nil) { [weak self] (result: Result<Bool>) -> Void in
                switch (result) {
                case .Success:
                    self?.delayList.insert(notification, at: 0)
                    
                    UserDefaults.standard.set(try? PropertyListEncoder().encode(self?.delayList), forKey: "notifications")
                    
                    self?.notificationTable.reloadData();
                    self?.delayTextElement.text = ""
                    break;
                case .Error(let error):
                    self?.errorLabel.isHidden = false;
                    self?.errorLabel.text = error.localizedDescription;
                    break;
                }
        }
    }
    
    private func enableSendButton(){
        self.sendNotificationButton.backgroundColor = UIColor(red: 89/255, green: 207/255, blue: 65/255, alpha: 1)
        self.sendNotificationButton.isEnabled = true;
    }
    
    private func disableSendButton(){
        self.sendNotificationButton.backgroundColor = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1);
        self.sendNotificationButton.isEnabled = false;
    }
    
    @objc private func settingsTapped() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet);
        
        let exit = UIAlertAction(title: "Log Out", style: .destructive) {
            action in
            self.user = nil;
            
            do {
                try Locksmith.deleteDataForUserAccount(userAccount: "MyAccount")
            } catch {
                
            }
            
            self.navigationController?.popViewController(animated: true)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        actionSheet.addAction(exit)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
    }
    
    private func getKeyboardDismissTapGesture() -> UITapGestureRecognizer? {
        guard let keyboardDismissTap = self.keyboardDismissTap else {
            self.keyboardDismissTap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard));
            return self.keyboardDismissTap;
        }
        
        return keyboardDismissTap;
    }
    
    private func registerDismissKeyboard() {
        guard let keyboardDismissTapGesture = self.getKeyboardDismissTapGesture() else {return}
        self.view.addGestureRecognizer(keyboardDismissTapGesture);
    }
    
    public func deregisterDismissKeyboard() {
        guard let keyboardDismissTapGesture = self.getKeyboardDismissTapGesture() else {return}
        self.view.removeGestureRecognizer(keyboardDismissTapGesture)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.deregisterDismissKeyboard();
        UIView.animate(withDuration: 0.1, animations: { () -> Void in self.view.frame.origin.y = 0; })
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    private func getKeyboardOpenFunction() -> ((_ offsetSize: CGFloat) -> Void) {
        return { [weak self] (_ offsetSize: CGFloat) -> Void in
            self?.registerDismissKeyboard();
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self?.view.frame.origin.y = -offsetSize;
            })
        }
    }
    
    private func getKeyboardHideFunction() -> (()->Void) {
        return { [weak self] () -> Void in
            self?.deregisterDismissKeyboard();
            UIView.animate(withDuration: 0.1, animations: { () -> Void in self?.view.frame.origin.y = 0; })
        }
    }
}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    internal func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.delayList.count;
    }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "tableCell")
        
        tableCell.textLabel?.numberOfLines = 0
        
        tableCell.detailTextLabel?.text = dateformatToStr.string(from: self.delayList[indexPath.row].date)
        tableCell.textLabel?.font = UIFont(name: "Avenir", size: 16)
        tableCell.textLabel?.text = self.delayList[indexPath.row].message
        
        
        return tableCell;
    }
    
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delayTextElement.text = self.delayList[indexPath.row].message
        self.sendButtonEnableControl();
        self.notificationTable.deselectRow(at: indexPath, animated: true)
    }
}
