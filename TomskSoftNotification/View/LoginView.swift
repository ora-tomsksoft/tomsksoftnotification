//
//  LoginView.swift
//  TomskSoftNotification
//
//  Created by ora on 7/2/18.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation
import SnapKit
import UIKit

class LoginView: UIScrollView {
    private(set) var view = UIView()
    private(set) var loginTxt = UITextField()
    private(set) var passwordTxt = UITextField()
    private(set) var errorLabel = UILabel()
    private(set) var loginButton = UIButton()
    private(set) var logo = UIImageView()
    private(set) var loginLabel = UILabel()
    private(set) var passwordLabel = UILabel()
    private var keyboardDismissTap: UITapGestureRecognizer?;
    
    func initialise() {
        initializeUI();
        createConstrains();
    }
    
    private func initializeUI() {
        view.addSubview(logo)
        view.addSubview(loginLabel)
        view.addSubview(passwordLabel)
        view.addSubview(loginTxt)
        view.addSubview(passwordTxt)
        view.addSubview(errorLabel)
        view.addSubview(loginButton)
        
        self.alwaysBounceVertical = true;
        
        addSubview(view)
        
        logo.image = UIImage(named: "logo-blue")
        
        loginLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        loginLabel.text = " Login"
        loginLabel.textAlignment = .center

        passwordLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        passwordLabel.text = " Password"
        passwordLabel.textAlignment = .center
        
        loginTxt.placeholder = "Tomsksoft domain login"
        loginTxt.layer.borderWidth = 1.0
        loginTxt.layer.cornerRadius = 5.0
        loginTxt.font = UIFont.systemFont(ofSize: 14.0)
        loginTxt.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        
        let loginPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: loginTxt.frame.height))
        
        loginTxt.leftView = loginPaddingView
        loginTxt.leftViewMode = UITextFieldViewMode.always
        loginTxt.addTarget(self, action: #selector(self.loginPrimaryAction), for: .primaryActionTriggered)
        loginTxt.addTarget(self, action: #selector(self.loginPasswordTextChanged), for: .editingChanged)

        passwordTxt.placeholder = "Password"
        passwordTxt.layer.borderWidth = 1.0
        passwordTxt.layer.cornerRadius = 5.0
        passwordTxt.font = UIFont.systemFont(ofSize: 14.0)
        passwordTxt.layer.borderColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
        
        let passwordPaddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: passwordTxt.frame.height))
        
        passwordTxt.leftView = passwordPaddingView
        passwordTxt.leftViewMode = UITextFieldViewMode.always
        passwordTxt.addTarget(self, action: #selector(passwordTxtPrimaryAction), for: .primaryActionTriggered)
        passwordTxt.addTarget(self, action: #selector(loginPasswordTextChanged), for: .editingChanged)
        passwordTxt.isSecureTextEntry = true

        errorLabel.textAlignment = .center
        errorLabel.font = UIFont.systemFont(ofSize: 11.0)
        errorLabel.textColor = UIColor.red
        errorLabel.isHidden = true

        loginButton.setTitle("Login", for: .normal)
        loginButton.titleLabel?.textAlignment = .center
        loginButton.titleLabel?.textColor = UIColor.white
        loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        loginButton.backgroundColor = UIColor(red: 55/255, green: 168/255, blue: 219/255, alpha: 1)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func createConstrains() {
        view.snp.makeConstraints { make in
            make.top.equalTo(self.snp.top).offset(0)
            make.left.equalTo(self.snp.left).offset(0)
            
            make.width.equalTo(self)
            make.height.equalTo(self)
        }
        
        logo.snp.makeConstraints { make in
            make.width.greaterThanOrEqualTo(40)
            make.height.greaterThanOrEqualTo(40)

            make.width.equalTo(logo.snp.height).multipliedBy(1 / 1)

            make.centerX.equalTo(view)
            make.top.equalTo(view.snp.top).offset(20)
            make.bottom.lessThanOrEqualTo(loginLabel.snp.top).offset(-40)
        }

        loginLabel.snp.makeConstraints { make in
            make.height.equalTo(20)
            make.width.greaterThanOrEqualTo(250)
            make.centerX.equalTo(view)
            make.bottom.equalTo(loginTxt.snp.top).offset(-10)
        }

        loginTxt.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.width.greaterThanOrEqualTo(250)
            make.left.equalTo(view.snp.left).offset(30)
            make.right.equalTo(view.snp.right).offset(-30)
            make.bottom.equalTo(passwordLabel.snp.top).offset(-10)
        }

        passwordLabel.snp.makeConstraints { make in
            make.height.equalTo(20)
            make.width.greaterThanOrEqualTo(250)
            make.centerX.equalTo(view)
            make.bottom.equalTo(passwordTxt.snp.top).offset(-10)
        }

        passwordTxt.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.width.greaterThanOrEqualTo(250)
            make.left.equalTo(view.snp.left).offset(30)
            make.right.equalTo(view.snp.right).offset(-30)
            make.bottom.equalTo(errorLabel.snp.top).offset(-5)
        }

        errorLabel.snp.makeConstraints { make in
            make.height.equalTo(15)
            make.width.greaterThanOrEqualTo(250)
            make.centerX.equalTo(view)
            make.bottom.equalTo(loginButton.snp.top).offset(-20)
        }

        loginButton.snp.makeConstraints { make in
            make.height.equalTo(40)
            make.width.greaterThanOrEqualTo(225)
            make.centerX.equalTo(view)
            make.bottom.equalTo(view.snp.bottom).offset(-30)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        registerDismissKeyboard()
        guard let userInfo = notification.userInfo else { return }
        guard var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue else {return}
        keyboardFrame = convert(keyboardFrame, from: nil)
        
        self.contentInset.bottom = keyboardFrame.size.height
        self.contentOffset.y = keyboardFrame.size.height
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        deregisterDismissKeyboard()
        self.contentInset.bottom = 0
    }
    
    private func getKeyboardDismissTapGesture() -> UITapGestureRecognizer? {
        guard let keyboardDismissTap = self.keyboardDismissTap else {
            self.keyboardDismissTap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard));
            return self.keyboardDismissTap;
        }
        
        return keyboardDismissTap;
    }
    
    @objc func dismissKeyboard() {
        self.endEditing(true)
    }
    
    private func registerDismissKeyboard() {
        guard let keyboardDismissTapGesture = self.getKeyboardDismissTapGesture() else {return}
        self.addGestureRecognizer(keyboardDismissTapGesture);
    }
    
    public func deregisterDismissKeyboard() {
        guard let keyboardDismissTapGesture = self.getKeyboardDismissTapGesture() else {return}
        self.removeGestureRecognizer(keyboardDismissTapGesture)
    }
    
    @objc private func loginPrimaryAction(_ sender: UITextField) {
        passwordTxt.becomeFirstResponder();
    }
    
    @objc func loginPasswordTextChanged(_ sender: UITextField) {
        hideError();
    }
    
    @objc func passwordTxtPrimaryAction(_ sender: UITextField) {
        endEditing(true)
        
        guard let login = loginTxt.text else { return; }
        guard let password = passwordTxt.text else { return; }
        
        if(login.isEmpty || password.isEmpty){
            return;
        }
        
        loginButton.sendActions(for: UIControlEvents.touchUpInside)
    }
    
    public func setError(_ error: String){
        errorLabel.text = error;
        errorLabel.isHidden = false;
    }
    
    public func hideError(){
        errorLabel.isHidden = true;
    }
}
