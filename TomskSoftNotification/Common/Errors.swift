//
//  Errors.swift
//  TomskSoftNotification
//
//  Created by ora on 18/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//


import UIKit

enum Errors {
  public enum NetworkErrors: Error {
        case NoConnectionError
        case ServerError
    }
    
    enum UserError: Error {
        case AuthorizationError
    }
    
    enum ServerErrors: Error {
        case Date_in_past
        case Time_not_correct
        case Unexpected_error
    }
}

extension Errors.NetworkErrors: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .NoConnectionError:
            return NSLocalizedString("No internet connection", comment: "");
            
        case .ServerError:
            return NSLocalizedString("Server Error", comment: "");
        }
    }
}

extension Errors.UserError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .AuthorizationError:
            return NSLocalizedString("Wrong login or password", comment: "");
        }
    }
}

extension Errors.ServerErrors: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .Date_in_past:
            return NSLocalizedString("Date is in the past", comment: "");
            
        case .Time_not_correct:
            return NSLocalizedString("Time of reminder is not correct", comment: "");
        
        
        case .Unexpected_error:
            return NSLocalizedString("Server Error", comment: "");
        }
    }
}
