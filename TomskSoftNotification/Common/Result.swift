//
//  Result.swift
//  TomskSoftNotification
//
//  Created by ora on 22/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

enum Result<T> {
    case Success(T)
    case Error(Error)
}
