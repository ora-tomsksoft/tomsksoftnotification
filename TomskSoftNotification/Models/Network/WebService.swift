//
//  WebService.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class WebService {
    
    private let notificationUrl: String = "https://extern.tomsksoft.com/user/note/set/";
    
    public func authRequest(_ login:String, _ password: String, _ callbackFunction: @escaping(_ result: Result<Bool>) -> Void) {
        let authString = self.getAuthString(login, password)
        let headers = ["Basic \(authString)":"Authorization"]
        
        self.postRequest(nil, headers){
            (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            if (error != nil) {
                callbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }

            guard let response = response else {
                callbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
            
            let serverResp = response as? HTTPURLResponse;

            if (serverResp?.statusCode == 401){
                callbackFunction(Result<Bool>.Error(Errors.UserError.AuthorizationError));
                return;
            }

            callbackFunction(Result<Bool>.Success(true));
            return;
        }
    }
    
    public func sendNotification(_ login: String, _ password: String, _ notification: Notification, _ remindDate: Date?, _ callbackFunction: ((Result<Bool>)->Void)?) {
        let pakage = JsonRPCRequestPakage(Methods.notification.rawValue, NotificationRequestParams(type: 1, message: notification.message, date: notification.date, remindDate: remindDate));
        
        let authString = self.getAuthString(login, password)
        let headers = ["Basic \(authString)":"Authorization"]
        self.postRequest(pakage, headers) {
            (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            guard let callbackFunction = callbackFunction else { return }
            
            let asyncCallbackFunction = { (result: Result<Bool>) -> Void in
                DispatchQueue.main.async {
                    callbackFunction(result)
                }
            }
            
            guard let data = data, error == nil else {
                asyncCallbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
            
            let serverResp = response as? HTTPURLResponse;
            if(response == nil || serverResp?.statusCode != 200){
                asyncCallbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
          
            if let respObject = try? JSONDecoder().decode(JsonRPCResponsePackage<Int>.self, from: data) {
                if(respObject.result == 1){
                    asyncCallbackFunction(Result<Bool>.Success(true));
                    return;
                }
                
                asyncCallbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
            
            guard let errorObject = try? JSONDecoder().decode(JsonRPCErrorPackage<ServerError>.self, from: data) else {
                asyncCallbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
            
            switch errorObject.error.code {
            case 1:
                asyncCallbackFunction(Result<Bool>.Error(Errors.ServerErrors.Date_in_past));
                return;
                
            case 2:
                asyncCallbackFunction(Result<Bool>.Error(Errors.ServerErrors.Time_not_correct));
                return;
                
            case 10:
                asyncCallbackFunction(Result<Bool>.Error(Errors.ServerErrors.Unexpected_error));
                return;
                
            default:
                asyncCallbackFunction(Result<Bool>.Error(Errors.NetworkErrors.ServerError));
                return;
            }
        }
    }
    
    private func getAuthString(_ username:String, _ password: String) -> String {
        let loginString = username + ":" + password;
        let loginData = loginString.data(using: String.Encoding.utf8)!
        return loginData.base64EncodedString();
    }
    
    private func postRequest(_ requestPakage: RequestDataProtocol?, _ headers: [String:String]?, _ callBack: ((Data?, URLResponse?, Error?)->Void)?){
        guard let url = URL(string: self.notificationUrl) else {
            return;
        }
        
        var request = URLRequest(url: url);
        request.httpMethod = "POST";
        
        if let headers = headers {
            for (key, value) in headers {
                request.setValue(key, forHTTPHeaderField: value);
            }
        }
        
        if let requestPakage = requestPakage {
            request.httpBody = requestPakage.getRequestData();
           // print("Request: " + String(data: request.httpBody!, encoding: String.Encoding.utf8)!)
        }
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let callBack = callBack {
                callBack(data, response, error);
            }
        }
        task.resume();
    }
    
    enum Methods : String {
        case notification
        case token_add
        case token_delete
    }
}
