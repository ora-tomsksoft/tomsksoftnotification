//
//  JsonRPCErrorPackage.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class JsonRPCErrorPackage <T: Codable> : BaseJsonRPCPakage {
    public var error: T;
    
    init(_ id: Int, error: T, version: JsonRPCProtocolVersions){
        self.error = error
        super.init(id, version)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        error = try container.decode(T.self, forKey: .error)
        try super.init(from: decoder)
    }
    
    private enum CodingKeys: String, CodingKey {
        case error
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(error, forKey: .error)
        
        try super.encode(to: encoder)
    }
}
