//
//  JsonRPCRequestPakage.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class JsonRPCRequestPakage <T: Codable>: BaseJsonRPCPakage {
    public var params: T;
    public var method: String;
    
    init(_ id: Int, _ method: String, _ params:T, _ version: JsonRPCProtocolVersions){
        self.params = params;
        self.method = method;
        super.init(id, version)
    }
    
    init(_ method: String, _ params: T){
        self.params = params;
        self.method = method;
        super.init(Int(arc4random_uniform(10000)), JsonRPCProtocolVersions.version_2)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        params = try container.decode(T.self, forKey: .params)
        method = try container.decode(String.self, forKey: .method)
        try super.init(from: decoder)
    }
    
    private enum CodingKeys: String, CodingKey {
        case params
        case method
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(params, forKey: .params)
        try container.encode(method, forKey: .method)
        
        try super.encode(to: encoder)
    }
}
