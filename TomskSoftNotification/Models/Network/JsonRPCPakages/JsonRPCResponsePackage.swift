//
//  JsonRPCResponsePackage.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class JsonRPCResponsePackage <T: Codable> : BaseJsonRPCPakage {
    public var result: T;
    
    init(_ id: Int, result: T, version: JsonRPCProtocolVersions){
        self.result = result
        super.init(id, version)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        result = try container.decode(T.self, forKey: .result)
        try super.init(from: decoder)
    }
    
    private enum CodingKeys: String, CodingKey {
        case result
    }
    
    override func encode(to encoder: Encoder) throws {
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(result, forKey: .result)
        
        try super.encode(to: encoder)
    }
}
