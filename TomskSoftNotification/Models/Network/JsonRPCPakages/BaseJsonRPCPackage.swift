//
//  JsonRPCRequestPakage.swift
//  TomskSoftNotification
//
//  Created by ora on 23/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class BaseJsonRPCPakage: Codable, RequestDataProtocol {
    
    public var JSONRPCProtocolVersion: JsonRPCProtocolVersions;
    public var id: Int;
    
    init(_ id: Int, _ version: JsonRPCProtocolVersions) {
        self.id = id
        self.JSONRPCProtocolVersion = version
    }
    
    enum JsonRPCProtocolVersions : String, Codable {
        case version_1 = "1.0"
        case version_2 = "2.0"
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case JSONRPCProtocolVersion = "jsonrpc"
    }
    
    required init(from decoder: Decoder) throws {
       let container = try decoder.container(keyedBy: CodingKeys.self)
       id = try container.decode(Int.self, forKey: .id)
       JSONRPCProtocolVersion = try container.decode(JsonRPCProtocolVersions.self, forKey: .JSONRPCProtocolVersion)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(JSONRPCProtocolVersion, forKey: .JSONRPCProtocolVersion)
    }
    
    public func getRequestData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
}
