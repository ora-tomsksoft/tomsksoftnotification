//
//  ServerError.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

struct ServerError: Codable {
    var code: Int;
    var message: String;
}
