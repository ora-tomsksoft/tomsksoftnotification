//
//  NotificationRequestParams.swift
//  TomskSoftNotification
//
//  Created by ora on 24/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

struct NotificationRequestParams: Codable {
    var type: Int;
    var message: String;
    var date: Date;
    var remindDate: Date?;
    
    func encode(to encoder: Encoder) throws {
        let formatter : DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(type, forKey: .type)
        try container.encode(message, forKey: .message)
        try container.encode(formatter.string(from: date), forKey: .date)
        
        guard let remindDate = self.remindDate else {
            try container.encode("", forKey: .remindDate)
            return;
        }
        
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        try container.encode(formatter.string(from: remindDate), forKey: .remindDate)
    }
}
