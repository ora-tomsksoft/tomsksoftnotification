//
//  User.swift
//  TomskSoftNotification
//
//  Created by ora on 18/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Locksmith
import Foundation

class User {
    private var login: String;
    private var password: String;
    
    init(_ login: String, _ password: String) {
        self.login = login;
        self.password = password;
    }
    
    convenience init?(){
        do {
            guard let credentials = try Locksmith.loadDataForUserAccount(userAccount: "MyAccount") else { return nil;}
            
            guard let login = credentials["login"] as? String else {return nil}
            guard let password = credentials["password"] as? String else {return nil}
            
            self.init(login, password)
        } catch {
            return nil;
        }
    }

    public func login(_ callbackFunction: ((Result<Bool>)->Void)?) {
        let webService = WebService();
        webService.authRequest(self.login, self.password) {
            [weak self] (result: Result<Bool>) -> Void in
            
            guard let self_obj = self else {
                return;
            }
            
            switch (result) {
                case .Success(let value):
                    if(value) {
                        do {
                            let credentials = try Locksmith.loadDataForUserAccount(userAccount: "MyAccount");
                            
                            if(credentials == nil) {
                                try Locksmith.saveData(data: ["login": self_obj.login, "password": self_obj.password], forUserAccount: "MyAccount")
                            }else{
                                try Locksmith.updateData(data: ["login": self_obj.login, "password": self_obj.password], forUserAccount: "MyAccount")
                            }
                        } catch {}
                    }
                    break;
                default:
                    break;
            }
            
            callbackFunction?(result);
        }
    }
    
    public func sendNotification(_ notification: Notification, _ remindDate: Date?, _ callbackFunction: ((Result<Bool>)->Void)?){
        let webService = WebService();
        webService.sendNotification(self.login, self.password, notification, remindDate, callbackFunction)
    }
    
}
