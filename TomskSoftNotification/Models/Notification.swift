//
//  Notification.swift
//  TomskSoftNotification
//
//  Created by ora on 6/25/18.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

struct Notification: Codable {
    var date: Date;
    var message: String;
}
