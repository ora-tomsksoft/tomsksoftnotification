//
//  LoginController.swift
//  TomskSoftNotification
//
//  Created by ora on 18/05/2018.
//  Copyright © 2018 ora. All rights reserved.
//

import Foundation

class LoginController {
    private var user: User?;
    
    public func login(_ login: String, _ password: String, _ callbackFunction: ((_ result: Result<Bool>)->Void)?) {
        self.user = User(login, password);
        self.user?.login() { (result: Result<Bool>) -> Void in
            guard let callbackFunction = callbackFunction else {return;}
            
            DispatchQueue.main.async {
                callbackFunction(result);
            }
        };
    }
    
    var loginUser: User? {
        get { return user;}
    }
}
